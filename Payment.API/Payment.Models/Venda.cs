﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public ICollection<Produto> Produtos { get; set; } = null!;
        public Vendedor Vendedor { get; set; } = null!;
        public DateTimeOffset Data { get; set; }
        public EnumStatus Status { get; set; }
    }
}
