﻿using Microsoft.AspNetCore.Mvc;
using Payment.API.Repositories;
using Payment.Models;

namespace Payment.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendaController : Controller
    {
        private readonly IVendaRepository _repository;

        public VendaController(IVendaRepository repository)
        {
            _repository = repository;
        }



        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var venda = _repository.Get(id);

            if (venda == null)
            {
                return NotFound();
            }

            return Ok(venda);
        }


        [HttpPost]
        public IActionResult Add([FromBody] Venda venda)
        {
            if (venda.Status != EnumStatus.aguardandoPagameto)
            {
                return BadRequest(new { Erro = "Permitido apenas cadastrar com o status inicial : 0 " });
            }
            _repository.Add(venda);
            return Ok(venda);
        }

        [HttpPut]
        public IActionResult Update([FromBody] Venda venda, int id)
        {
            
            if (
                venda.Status == EnumStatus.pagamentoAprovado ||
                venda.Status == EnumStatus.enviadoParaTransportadora ||
                venda.Status == EnumStatus.cancelada ||
                venda.Status == EnumStatus.entregue ||
                venda.Status == EnumStatus.aguardandoPagameto
                )
            {

                _repository.Update(venda);
            }
            else
            {
                return BadRequest(new { Erro = "Status venda não contém" });

            }

            var vendaSta = _repository.Get(venda.Id);

            if (vendaSta.Status != venda.Status)
            {
                return BadRequest(new { Erro = "Regras de negócio não possibilita mudar para esse status! " });
            }
            else
            {
                return Ok(vendaSta);

            }

        }
    }
}
