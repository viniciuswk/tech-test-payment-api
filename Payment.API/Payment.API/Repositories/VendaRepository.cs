﻿using Payment.Models;

namespace Payment.API.Repositories
{
    public class VendaRepository : IVendaRepository
    {

        public static List<Venda> _db = new List<Venda>();

        public void Add(Venda venda)
        {
            _db.Add(venda);
        }

        public Venda Get(int id)
        {
            return _db.Find(x => x.Id == id)!;
        }

        public void Update(Venda venda)
        {
            var minhaVenda = Get(venda.Id);
            // _db.Remove(Get(venda.Id));
            var meuStatusVenda = venda.Status;

            if (minhaVenda.Status == EnumStatus.aguardandoPagameto)
            {
                if (meuStatusVenda == EnumStatus.pagamentoAprovado || meuStatusVenda == EnumStatus.cancelada)
                {
                    minhaVenda.Status = meuStatusVenda;
                    Console.WriteLine(minhaVenda);
                }
                
            }
            else if (minhaVenda.Status == EnumStatus.pagamentoAprovado)
            {
                if (meuStatusVenda == EnumStatus.enviadoParaTransportadora || meuStatusVenda == EnumStatus.cancelada)
                {
                    minhaVenda.Status = meuStatusVenda;
                    Console.WriteLine(minhaVenda);
                }
            }
            else if (minhaVenda.Status == EnumStatus.enviadoParaTransportadora)
            {
                if (meuStatusVenda == EnumStatus.entregue)
                {
                    minhaVenda.Status = meuStatusVenda;
                    Console.WriteLine(minhaVenda);
                }
            }
            else
            {
                throw new Exception("Não pode ser alterado para esse status");
            }
            
            

            _db.Remove(Get(venda.Id));
            _db.Add(minhaVenda);

        }


    }
}
