﻿using Payment.Models;

namespace Payment.API.Repositories
{
    public interface IVendaRepository
    {
        Venda Get(int id);
        void Add(Venda venda);
        void Update(Venda venda);
    }
}
